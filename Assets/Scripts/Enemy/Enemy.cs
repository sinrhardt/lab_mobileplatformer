using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static Action<int> OnPlayerCollision;

    
    private float _walkingSpace = 5.0f;
    private float _currentSpeed = 5.0f;
    private float _temp = 0.0f;
    private int _score = 250;

    public int Score => _score;

    private int _damage = 1;
    

    void Update()
    {
        if (_temp >= _walkingSpace)
        {
            _currentSpeed *= -1;
            _temp = 0.0f;
        }

        
        transform.position += new Vector3(_currentSpeed * Time.deltaTime, 0, 0);
        _temp += Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        OnPlayerCollision?.Invoke(_damage);
        _currentSpeed *= -1;
    }
    
}
