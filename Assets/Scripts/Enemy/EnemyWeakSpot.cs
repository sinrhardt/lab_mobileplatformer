using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeakSpot : MonoBehaviour
{
    public static Action<int> OnPlayerScore;
    private int score;

    private void Start()
    {
        score = GetComponentInParent<Enemy>().Score;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        OnPlayerScore?.Invoke(score);
        transform.parent.gameObject.SetActive(false);
    }
}
