using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    [SerializeField] private int health = 5;
    
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI healthText;

    private int _scoreTracker;
    private int _healthTracker;

    private bool _canTakeDamage = true;
    private float _damageCooldown = 0.5f;
    
    private void OnEnable()
    {
        EnemyWeakSpot.OnPlayerScore += UpdateScore;
        Enemy.OnPlayerCollision += TakeDamage;
    }
    
    private void OnDisable()
    {
        EnemyWeakSpot.OnPlayerScore -= UpdateScore;
        Enemy.OnPlayerCollision -= TakeDamage;
    }

    private void Start()
    {
        _scoreTracker = 0;
        UpdateScoreUI();
        _healthTracker = health;
        UpdateHealthUI();
    }

    private void UpdateScore(int score)
    {
        _scoreTracker += score;
        UpdateScoreUI();
    }

    private void TakeDamage(int damage)
    {
        if (!_canTakeDamage) return;

            _healthTracker -= damage;

        if (health <= 0)
        {
            // TO-DO
            // lose
            return;
        }
        UpdateHealthUI();
        _canTakeDamage = false;
        StartCoroutine(HitImmunity());
    }

    private IEnumerator HitImmunity()
    {
        yield return new WaitForSeconds(_damageCooldown);
        _canTakeDamage = true;
    }

    private void UpdateScoreUI()
    {
        scoreText.SetText(_scoreTracker.ToString("00000"));
    }
    
    private void UpdateHealthUI()
    {
        string temp = "";
        for (int i = 0; i < _healthTracker; i++)
        {
            temp += "*";
        }

        healthText.SetText(temp);
    }
}
