using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float maxVelocity = 5.0f;
    [SerializeField] private float maxAcceleration = 1.0f;
    [SerializeField] private float maxDeceleration = 1.0f;
    [SerializeField] private float maxTurnSpeed = 1.0f;
    [Header("Jump")]
    [SerializeField] private float jumpForce = 2.5f;
    [SerializeField] private int maxJumps = 2;
    
    private float _currentInput, _desiderVelocity, _maxSpeedChange;
    private Vector2 _velocity;
    private Rigidbody2D _rb;

    private bool _grounded;
    private int jumpLeft;
    private void OnEnable()
    {
        InputManager.OnMovementInput += LateralInput;
        InputManager.OnJumpInput += VerticalMovement;
    }

    private void OnDisable()
    {
        InputManager.OnMovementInput -= LateralInput;
        InputManager.OnJumpInput -= VerticalMovement;

    }

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        jumpLeft = maxJumps;
    }

    private void FixedUpdate()
    {
        LateralMovement();


        _rb.velocity = _velocity;
    }


    private void OnCollisionEnter2D(Collision2D col)
    {
        _grounded = true;
        jumpLeft = maxJumps;
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        _grounded = false;
    }


    private void LateralInput(float direction)
    {
        _currentInput = direction;
    }
    
    

    #region Movement

    private void LateralMovement()
    {
        _velocity = _rb.velocity;

        if (_currentInput != 0)
        {
            if (Mathf.Sign(_currentInput) != Mathf.Sign(_velocity.x))
            {
                _maxSpeedChange = maxTurnSpeed * Time.deltaTime;
            }
            else
            {
                _maxSpeedChange = maxAcceleration * Time.deltaTime;
            }
        }
        else
            _maxSpeedChange = maxDeceleration * Time.deltaTime;
        
        _velocity.x = Mathf.MoveTowards(_velocity.x, _currentInput * maxVelocity, _maxSpeedChange);
    }

    private void VerticalMovement()
    {
        if (_grounded || jumpLeft > 0)
        {
            _rb.AddForce(new Vector2(0, jumpForce));
            jumpLeft--;
        }
    }

    #endregion
    

    
}
