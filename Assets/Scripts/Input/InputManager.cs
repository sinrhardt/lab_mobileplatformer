using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    
    private float _touchStartPos;
    private float _touchEndPos;
    private float _directionValue;

    public static Action<float> OnMovementInput;
    public static Action OnJumpInput;


    public void RightInput()
    {
        OnMovementInput?.Invoke(1);
    }
    
    public void LeftInput()
    {
        OnMovementInput?.Invoke(-1);
    }

    public void ReleaseInput()
    {
        OnMovementInput?.Invoke(0);
    }

    public void JumpInput()
    {
        OnJumpInput?.Invoke();
    }
}
